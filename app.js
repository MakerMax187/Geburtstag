const App = {
    data() {
        return {
            // Liste mit allen Datensätzen
            kontakte: [],
            /* 
            [
                { name: 'Anna', geburtsdatum: '1964-12-20' },
                { name: 'Berta', geburtsdatum: '1973-03-13' },
                { name: 'Carla', geburtsdatum: '1977-07-07' },
                { name: 'Dieter', geburtsdatum: '3666-12-31' }
            ],
            */

            // Sichtbarkeit der Views
            readVisible: true,
            createVisible: false,
            deleteVisible: false,
            updateVisible: false,

            // Create View
            kontaktNeu: {},

            // Delete View
            kontaktDelete: {},

            // Update View
            kontaktUpdate: {},

            // zum Speichern, welcher Kontakt bei 'Edit'
            // ausgewählt wurde
            kontaktZwischenspeicher: {}
        }
    },

    methods: {
        // ------------------------------------------
        // Read View
        // ------------------------------------------
        buttonNeuClick() {
            // Standardwerte für neuen Kontakt setzen
            this.kontaktNeu = {
                name: '',
                geburtsdatum: '2000-01-01',
                geschenkidee: 'keine Ahnung'
            };

            // Create View anzeigen
            this.showCreateView();
        },

        buttonEditClick(kontakt) {
            // Update View anzeigen
            this.showUpdateView();

            // Daten des Kontakts anzeigen (Kopie von kontakt)            
            this.kontaktUpdate.name = kontakt.name;
            this.kontaktUpdate.geburtsdatum = kontakt.geburtsdatum;
            this.kontaktUpdate.geschenkidee = kontakt.geschenkidee;

            // Merken, welcher Kontakt angeklickt wurde
            this.kontaktZwischenspeicher = kontakt;
        },

        buttonLoeschenClick(kontakt) {
            // Delete View anzeigen
            this.showDeleteView();

            // Namen des Kontakts ausgeben
            this.kontaktDelete = kontakt;
        },

        // ------------------------------------------
        // Create View
        // ------------------------------------------
        buttonSpeichernClick() {
            // Neuen Datensatz speichern            
            this.kontakte.push(this.kontaktNeu);
            this.speichern();

            // Read View anzeigen
            this.showReadView();
        },

        // ------------------------------------------
        // Delete View
        // ------------------------------------------
        buttonDeleteJaClick() {
            // Kontakt löschen
            // index im Array ermitteln
            const index = this.kontakte.indexOf(this.kontaktDelete);
            // Element aus Array löschen
            this.kontakte.splice(index, 1);
            // in localStorage speichern
            this.speichern();

            // Read View anzeigen
            this.showReadView();
        },

        // ------------------------------------------
        // Update View
        // ------------------------------------------
        buttonUpdateSpeichernClick() {
            // geänderten Daten speichern (Kopie)            
            this.kontaktZwischenspeicher.name = this.kontaktUpdate.name;
            this.kontaktZwischenspeicher.geburtsdatum = this.kontaktUpdate.geburtsdatum;
            this.kontaktZwischenspeicher.geschenkidee = this.kontaktUpdate.geschenkidee;
            // in localStorage speichern
            this.speichern();

            // Read View anzeigen
            this.showReadView();
        },

        // ------------------------------------------
        // Abbrechen für alle Views
        // ------------------------------------------
        buttonAbbrechenClick() {
            // Read View anzeigen
            this.showReadView();
        },

        // ------------------------------------------
        // show Views
        // ------------------------------------------
        showReadView() {
            this.readVisible = true;
            this.createVisible = false;
            this.deleteVisible = false;
            this.updateVisible = false;
        },

        showCreateView() {
            this.readVisible = false;
            this.createVisible = true;
            this.deleteVisible = false;
            this.updateVisible = false;
        },

        showDeleteView() {
            this.readVisible = false;
            this.createVisible = false;
            this.deleteVisible = true;
            this.updateVisible = false;
        },

        showUpdateView() {
            this.readVisible = false;
            this.createVisible = false;
            this.deleteVisible = false;
            this.updateVisible = true;
        },

        // ------------------------------------------
        // Hilfsmethoden
        // ------------------------------------------
        deutschesDatumsformat(rfcDatum) {
            const dateObject = new Date(rfcDatum);
            const options = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            };
            const dateString = dateObject.toLocaleDateString('de-DE', options);
            return dateString;
        },

        // ------------------------------------------
        // localStorage
        // ------------------------------------------
        speichern() {
            const jsonString = JSON.stringify(this.kontakte);
            localStorage.setItem('geburtstage', jsonString);
        },

        laden() {
            const jsonString = localStorage.getItem('geburtstage');
            if (jsonString) {
                // es gibt schon etwas im Speicher
                this.kontakte = JSON.parse(jsonString);
            } else {
                // nichts gespeichert
                this.kontakte = [];
            }
        }
    },

    mounted() {
        // Wird beim Start des Programms ausgeführt
        this.laden();
    }
};
Vue.createApp(App).mount('#app');